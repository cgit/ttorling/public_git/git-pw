# Enable Python 3 only for Fedora
%global with_python3 1

Name:           git-pw
Version:        1.1.1
Release:        1%{?dist}
Summary:        A tool for integrating Git with Patchwork

Group:          Development/Tools
License:        MIT
URL:            https://github.com/getpatchwork/%{name}
Source0:        https://files.pythonhosted.org/packages/source/g/%{name}/%{name}-%{version}.tar.gz

BuildArch:      noarch

%if 0%{?with_python3}
BuildRequires:  python3-devel
BuildRequires:  python3-pbr
BuildRequires:  python3-setuptools
%else
BuildRequires:  python2-devel
BuildRequires:  python2-pbr
BuildRequires:  python2-setuptools
%endif

Requires:       git
%if 0%{?with_python3}
Requires:       python3-arrow
Requires:       python3-click
Requires:       python3-requests
Requires:       python3-setuptools
Requires:       python3-tabulate
%else
Requires:       python2-arrow
Requires:       python2-click
Requires:       python2-requests
Requires:       python2-setuptools
Requires:       python2-tabulate
%endif

%description
git-pw is a tool for integrating Git with Patchwork, the web-based patch
tracking system.

Important

git-pw only supports Patchwork 2.0+ and REST API support must be enabled on
the server end. You can check for support by browsing /about for your given
instance. If this page returns a 404, you are using Patchwork < 2.0.

The pwclient utility can be used to interact with older Patchwork instances
or instances with the REST API disabled.

%prep
%setup -q

%build
%if 0%{?with_python3}
%py3_build
%else
%py2_build
%endif

%install
%if 0%{?with_python3}
%py3_install
%else
%py2_install
%endif

%files
%defattr(-,root,root,-)
%license LICENSE
%doc README.rst
%{_bindir}/git-pw
# Our package name is git-pw, but setup.py installs with underscore.
%if 0%{?with_python3}
%{python3_sitelib}/git_pw/
%{python3_sitelib}/git_pw-%{version}-py%{python3_version}*.egg-info/
%else
%{python2_sitelib}/git_pw/
%{python2_sitelib}/git_pw-%{version}-py%{python2_version}*.egg-info/
%endif

%changelog
* Fri Mar 16 2018 Tim Orling <ticotimo@gmail.com> - 1.1.1-1
- Initial spec for Fedora
